package me.randomhashtags.randompackage.api.unfinished;

import me.randomhashtags.randompackage.RandomPackageAPI;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class Duels extends RandomPackageAPI implements Listener, CommandExecutor {

    private static Duels instance;
    public static final Duels getDuels() {
        if(instance == null) instance = new Duels();
        return instance;
    }

    public boolean isEnabled;
    public YamlConfiguration config;


    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "duels.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;

        sendConsoleMessage("&6[RandomPackage] &aLoaded Duels &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        config = null;
        HandlerList.unregisterAll(this);
    }
}
