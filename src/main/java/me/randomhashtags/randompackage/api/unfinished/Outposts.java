package me.randomhashtags.randompackage.api.unfinished;

import me.randomhashtags.randompackage.RandomPackageAPI;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class Outposts extends RandomPackageAPI implements Listener, CommandExecutor {

    private static Outposts instance;
    public static final Outposts getOutposts() {
        if(instance == null) instance = new Outposts();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save(null, "outposts.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;

        sendConsoleMessage("&6[RandomPackage] &aLoaded Outposts &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        config = null;
        HandlerList.unregisterAll(this);
    }
}
