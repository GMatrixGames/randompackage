package me.randomhashtags.randompackage.utils.classes.globalchallenges;

import me.randomhashtags.randompackage.api.GlobalChallenges;
import me.randomhashtags.randompackage.utils.universal.UVersion;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class GlobalChallenge extends UVersion {
	private static GlobalChallenges globalchallenges;
	public static HashMap<String, GlobalChallenge> challenges;
	public String path, name, tracks;
	private ItemStack display;
	public List<UUID> participants;
	public long started;

	public GlobalChallenge(String path, String name, String tracks, ItemStack display, List<UUID> participants) {
		if(globalchallenges == null) {
			globalchallenges = GlobalChallenges.getChallenges();
			challenges = new HashMap<>();
		}
		this.path = path;
		this.name = name;
		this.tracks = tracks;
		this.display = display;
		this.participants = participants;
		challenges.put(path, this);
	}
	public ItemStack getDisplayItem() { return display.clone(); }
	public ActiveGlobalChallenge start() {
		return start(System.currentTimeMillis(), new HashMap<>());
	}
	public ActiveGlobalChallenge start(long started, HashMap<UUID, Double> participants) {
		final HashMap<GlobalChallenge, ActiveGlobalChallenge> a = ActiveGlobalChallenge.active;
		return a != null ? a.getOrDefault(this, new ActiveGlobalChallenge(started, this, participants)) : new ActiveGlobalChallenge(started, this, participants);
	}
	public boolean isActive() {
		final HashMap<GlobalChallenge, ActiveGlobalChallenge> a = ActiveGlobalChallenge.active;
		return a != null && a.containsKey(this);
	}

	public static GlobalChallenge valueOf(String pathORname) {
		if(challenges != null) {
			for(GlobalChallenge g : challenges.values()) {
				if(g.name.equals(pathORname) || g.path.equals(pathORname)) {
					return g;
				}
			}
		}
		return null;
	}

	public static void deleteAll() {
		globalchallenges = null;
		challenges = null;
	}
}
