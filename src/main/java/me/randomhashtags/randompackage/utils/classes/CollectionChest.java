package me.randomhashtags.randompackage.utils.classes;

import me.randomhashtags.randompackage.utils.universal.UMaterial;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class CollectionChest {
	
	public static final ArrayList<CollectionChest> chests = new ArrayList<>();
	public static final HashMap<CollectionChest, HashMap<ItemStack, Integer>> storage = new HashMap<>();

	public UUID uuid;
	public UMaterial filter;
	public final String placer;
	public final Location location;
	public CollectionChest(String placer, Location location, UMaterial filter) {
		this.uuid = UUID.randomUUID();
		this.placer = placer;
		this.filter = filter;
		this.location = location;
		chests.add(this);
		storage.put(this, new HashMap<>());
	}
	public void setFilter(UMaterial newfilter) {
		filter = newfilter;
	}
	public void destroy() {
		for(ItemStack is : storage.get(this).keySet()) {
			location.getWorld().dropItemNaturally(location, new ItemStack(is.getType(), storage.get(this).get(is), is.getData().getData()));
		}
		delete();
	}
	public void delete() { chests.remove(this); storage.remove(this); }
	
	public static CollectionChest valueOf(Block block) {
		for(CollectionChest cc : chests) {
			if(cc.location.equals(block.getLocation())) return cc;
		}
		return null;
	}
	public static void remove(CollectionChest cc) { chests.remove(cc); storage.remove(cc); }
}
