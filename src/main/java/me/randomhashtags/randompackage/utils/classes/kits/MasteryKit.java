package me.randomhashtags.randompackage.utils.classes.kits;

import me.randomhashtags.randompackage.RandomPackageAPI;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MasteryKit {
    public static HashMap<String, MasteryKit> kits;
    private static RandomPackageAPI api;
    private YamlConfiguration yml;
    private String ymlName, fallenheroname;
    private FallenHero fallenhero;
    private int slot;
    private long cooldown;
    private ItemStack item, fallenherogem, fallenherospawnitem;
    private HashMap<Object, Integer> requiredKits;
    private boolean losesKitsUponUnlocking;
    public MasteryKit(File f) {
        if(kits == null) {
            kits = new HashMap<>();
            api = RandomPackageAPI.getAPI();
        }
        this.yml = YamlConfiguration.loadConfiguration(f);
        this.ymlName = f.getName().split("\\.yml")[0];
        slot = yml.getInt("gui settings.slot");
        kits.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public FallenHero getFallenHero() {
        if(fallenhero == null) fallenhero = FallenHero.heroes.get(yml.getString("settings.fallen hero"));
        return fallenhero;
    }
    public ItemStack getFallenHeroSpawnItem() {
        if(fallenherospawnitem == null) {
            final String n = getItem().getItemMeta().getDisplayName();
            final ItemStack is = getFallenHero().getSpawnItem();
            final ItemMeta m = is.getItemMeta();
            m.setDisplayName(m.getDisplayName().replace("{NAME}", n));
            final List<String> l = new ArrayList<>();
            for(String s : m.getLore()) {
                l.add(s.replace("{NAME}", n));
            }
            m.setLore(l);
            is.setItemMeta(m);
            fallenherospawnitem = is;
        }
        return fallenherospawnitem.clone();
    }
    public ItemStack getFallenHeroGem() {
        if(fallenherogem == null) {
            final String n = getItem().getItemMeta().getDisplayName();
            final ItemStack is = getFallenHero().getGem();
            final ItemMeta m = is.getItemMeta();
            m.setDisplayName(m.getDisplayName().replace("{NAME}", n));
            final List<String> l = new ArrayList<>();
            for(String s : m.getLore()) {
                l.add(s.replace("{NAME}", n));
            }
            m.setLore(l);
            is.setItemMeta(m);
            fallenherogem = is;
        }
        return fallenherogem.clone();
    }
    public String getFallenHeroName() {
        if(fallenheroname == null) fallenheroname = getFallenHero().getName().replace("{NAME}", getItem().getItemMeta().getDisplayName());
        return fallenheroname;
    }
    public int getSlot() { return slot; }
    public long getCooldown() {
        if(cooldown == 0) cooldown = yml.getLong("settings.cooldown");
        return cooldown;
    }
    public ItemStack getItem() {
        if(item == null) item = api.d(yml, "gui settings");
        return item.clone();
    }
    public HashMap<Object, Integer> getRequiredKits() {
        if(requiredKits == null) {
            requiredKits = new HashMap<>();
            final List<String> R = yml.getStringList("required kits");
            for(String s : R) {
                final String[] a = s.split(";");
                final String K = a[1];
                Object kit = null;
                if(a[0].equals("gkit")) kit = GlobalKit.kits.get(K);
                else if(a[1].equals("vkit")) kit = EvolutionKit.kits.get(K);
                requiredKits.put(kit, Integer.parseInt(a[2]));
            }
        }
        return requiredKits;
    }
    public boolean losesKitsUponUnlocking() { return losesKitsUponUnlocking; }

    public static MasteryKit valueOf(int slot) {
        for(MasteryKit m : kits.values())
            if(m.getSlot() == slot)
                return m;
        return null;
    }

    public static void deleteAll() {
        kits = null;
        api = null;
    }
}
