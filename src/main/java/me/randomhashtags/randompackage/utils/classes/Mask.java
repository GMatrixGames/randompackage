package me.randomhashtags.randompackage.utils.classes;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import me.randomhashtags.randompackage.RandomPackageAPI;
import me.randomhashtags.randompackage.utils.universal.UMaterial;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Mask {
    private static HashMap<String, ItemStack> maskOwners;
    public static HashMap<String, Mask> masks;
    private static RandomPackageAPI api;

    private YamlConfiguration yml;
    private String ymlName, owner;
    private List<String> addedLore, attributes;
    private ItemStack item;

    public Mask(File f) {
        if(masks == null) {
            maskOwners = new HashMap<>();
            masks = new HashMap<>();
            api = RandomPackageAPI.getAPI();
        }
        this.yml = YamlConfiguration.loadConfiguration(f);
        this.ymlName = f.getName().split("\\.yml")[0];

        final String tex = yml.getString("texture");
        owner = tex != null ? tex : yml.getString("owner");
        item = api.d(yml, "item");
        if(item != null) {
            if(maskOwners.containsKey(owner)) {
                item = maskOwners.get(owner);
            } else if(owner.startsWith("http")) {
                final ItemMeta im = item.getItemMeta();
                item = getSkull(owner, im.getDisplayName(), im.getLore());
            } else {
                final SkullMeta sm = (SkullMeta) item.getItemMeta();
                sm.setOwner(owner);
                item.setItemMeta(sm);
                maskOwners.put(owner, item);
            }
        }
        masks.put(ymlName, this);
    }

    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public String getOwned() { return owner; }
    public ItemStack getItem() {
        return item.clone();
    }
    public List<String> getAddedLore() {
        if(addedLore == null) addedLore = api.colorizeListString(yml.getStringList("added lore"));
        return addedLore;
    }
    public List<String> getAttributes() {
        if(attributes == null) attributes = yml.getStringList("attributes");
        return attributes;
    }

    // https://www.spigotmc.org/threads/tutorial-player-skull-with-custom-skin.143323/ , edited by RandomHashTags
    private ItemStack getSkull(String skinURL, String name, List<String> lore) {
        final ItemStack head = UMaterial.PLAYER_HEAD_ITEM.getItemStack();
        if(skinURL.isEmpty()) return head;
        final SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        headMeta.setDisplayName(name);
        headMeta.setLore(lore);
        final GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        final byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", skinURL).getBytes());
        profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        Field profileField = null;
        try {
            profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        head.setItemMeta(headMeta);
        return head;
    }

    public static Mask valueOf(ItemStack is) {
        if(is != null && is.hasItemMeta()) {
            for(Mask m : masks.values()) {
                final ItemStack i = m.getItem();
                if(i.isSimilar(is))
                    return m;
            }
        }
        return null;
    }
    public static Mask getOnItem(ItemStack is) {
        final ItemMeta im = is != null ? is.getItemMeta() : null;
        if(im != null && im.hasLore()) {
            final List<String> l = im.getLore();
            for(Mask m : masks.values())
                if(l.containsAll(m.getAddedLore()))
                    return m;
        }
        return null;
    }

    public static void deleteAll() {
        masks = null;
    }
}
