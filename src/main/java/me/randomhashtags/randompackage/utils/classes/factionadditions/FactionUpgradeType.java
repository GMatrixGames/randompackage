package me.randomhashtags.randompackage.utils.classes.factionadditions;

import java.util.HashMap;
import java.util.List;

public class FactionUpgradeType {
	public static HashMap<String, FactionUpgradeType> types;
	public final String path, perkAchievedPrefix, perkUnachievedPrefix, requirementsPrefix;
	public final List<String> unlock, upgrade, maxed, format;
	public final boolean itemAmountEqualsTier;
	public FactionUpgradeType(String path, String perkAchievedPrefix, String perkUnachievedPrefix, String requirementsPrefix, List<String> unlock, List<String> upgrade, List<String> maxed, List<String> format, boolean itemAmountEqualsTier) {
		if(types == null) {
			types = new HashMap<>();
		}
		this.path = path;
		this.perkAchievedPrefix = perkAchievedPrefix;
		this.perkUnachievedPrefix = perkUnachievedPrefix;
		this.requirementsPrefix = requirementsPrefix;
		this.unlock = unlock;
		this.upgrade = upgrade;
		this.maxed = maxed;
		this.format = format;
		this.itemAmountEqualsTier = itemAmountEqualsTier;
		types.put(path, this);
	}

	public static void deleteAll() {
		types = null;
	}
}
