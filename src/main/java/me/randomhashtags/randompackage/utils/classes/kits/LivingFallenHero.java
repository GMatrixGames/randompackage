package me.randomhashtags.randompackage.utils.classes.kits;

import me.randomhashtags.randompackage.utils.universal.UVersion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.*;

public class LivingFallenHero {
    public static HashMap<UUID, LivingFallenHero> living;
    private static UVersion uv;
    private Object kit;
    private FallenHero type;
    private LivingEntity fallenhero;
    private UUID summoner, fallenherouuid;
    private Location spawnedLocation;
    public LivingFallenHero(Object kit, FallenHero type, UUID summoner, Location spawnedLocation) {
        if(living == null) {
            living = new HashMap<>();
            uv = UVersion.getUVersion();
        }
        this.kit = kit;
        this.type = type;
        this.summoner = summoner;
        this.spawnedLocation = spawnedLocation;
        fallenhero = uv.getEntity(type.getType(), spawnedLocation, true);
        final String N = kit instanceof GlobalKit ? ((GlobalKit) kit).getItem().getItemMeta().getDisplayName() : kit instanceof EvolutionKit ? ((EvolutionKit) kit).getItem().getItemMeta().getDisplayName() : ((MasteryKit) kit).getItem().getItemMeta().getDisplayName();
        fallenhero.setCustomName(type.getName().replace("{NAME}", N));
        fallenherouuid = fallenhero.getUniqueId();
        fallenhero.addPotionEffects(type.getPotionEffects());
        living.put(fallenherouuid, this);
    }
    public Object getKit() { return kit; }
    public FallenHero getType() { return type; }
    public LivingEntity getFallenHero() { return fallenhero; }
    public UUID getSummoner() { return summoner; }
    public Location getSpawnedLocation() { return spawnedLocation; }

    public void delete() {
        fallenhero.remove();
        killed(null);
    }
    public void killed(EntityDeathEvent event) {
        if(event != null) {
            event.setDroppedExp(0);
            event.getDrops().clear();

            final World w = fallenhero.getWorld();
            if(uv.random.nextInt(100) <= type.getGemDropChance()) {
                final HashMap<String, String> r = new HashMap<>();
                r.put("{PLAYER}", event.getEntity().getKiller().getName());
                r.put("{NAME}", fallenhero.getCustomName());
                for(String s : type.getReceiveKitMsg()) {
                    for(String re : r.keySet()) s = s.replace(re, r.get(re));
                    Bukkit.broadcastMessage(s);
                }
                w.dropItem(fallenhero.getLocation(), kit instanceof GlobalKit ? (((GlobalKit) kit).getFallenHeroGem()) : kit instanceof EvolutionKit ? ((EvolutionKit) kit).getFallenHeroGem() : ((MasteryKit) kit).getFallenHeroGem());
            } else {
                //final List<KitItem> items = new ArrayList<>(gkit != null ? gkit.getItems() : vkit.getItems());
                //w.dropItem(l, api.d(yml, "items." + items.get(random.nextInt(items.size())).path, random.nextInt(gkit != null ? gkit.getMaxTier() : vkit.getMaxLevel())));
            }
        }

        living.remove(fallenherouuid);
        kit = null;
        type = null;
        fallenhero = null;
        fallenherouuid = null;
        summoner = null;
        spawnedLocation = null;
        if(living.isEmpty()) {
            living = null;
            uv = null;
        }
    }

    public static void deleteAll() {
        living = null;
        uv = null;
    }
}
