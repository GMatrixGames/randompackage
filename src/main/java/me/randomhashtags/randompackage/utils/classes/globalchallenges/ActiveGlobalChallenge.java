package me.randomhashtags.randompackage.utils.classes.globalchallenges;

import me.randomhashtags.randompackage.RandomPackage;
import me.randomhashtags.randompackage.api.GlobalChallenges;
import me.randomhashtags.randompackage.api.events.globalchallenges.GlobalChallengeEndEvent;
import me.randomhashtags.randompackage.utils.RPPlayer;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

import java.util.*;

public class ActiveGlobalChallenge {
    public static HashMap<GlobalChallenge, ActiveGlobalChallenge> active;
    private static PluginManager pm;
    private static GlobalChallenges globalchallenges;
    private GlobalChallenge type;
    private HashMap<UUID, Double> participants;
    private int task;
    private long started;

    public ActiveGlobalChallenge(long started, GlobalChallenge type, HashMap<UUID, Double> participants) {
        if(active == null) {
            active = new HashMap<>();
            pm = Bukkit.getPluginManager();
            globalchallenges = GlobalChallenges.getChallenges();
        }
        this.started = started;
        this.type = type;
        this.participants = participants;
        long remainingTime = getRemainingTime();
        if(remainingTime < 0) remainingTime = 0;
        task = Bukkit.getScheduler().scheduleSyncDelayedTask(RandomPackage.getPlugin, () -> end(true, 3), remainingTime);
        active.put(type, this);
    }

    public long getStartedTime() { return started; }
    public GlobalChallenge getType() { return type; }
    public HashMap<UUID, Double> getParticipants() { return participants; }

    public long getRemainingTime() {
        final String t = type.tracks.split(";")[1].toLowerCase();
        final double days = t.contains("d") ? Double.parseDouble(t.split("d")[0])*60*60*24*1000 : 0, hrs = t.contains("h") ? Double.parseDouble(t.split("h")[0])*60*60*1000 : 0, min = t.contains("m") ? Double.parseDouble(t.split("m")[1])*60*1000 : 0, sec = t.contains("s") ? Double.parseDouble(t.split("s")[1])*1000 : 0;
        final long ttl = Double.valueOf(started+days+hrs+min+sec).longValue();
        return ttl-System.currentTimeMillis();
    }
    public void increaseValue(UUID player, double value) {
        participants.put(player, participants.getOrDefault(player, 0.00)+value);
    }
    public double getValue(UUID player) {
        return participants.getOrDefault(player, 0.00);
    }
    public void setValue(UUID player, double value) {
        participants.put(player, value);
    }

    public void end(boolean giveRewards, int recordPlacements) {
        final GlobalChallengeEndEvent e = new GlobalChallengeEndEvent(this, giveRewards);
        pm.callEvent(e);
        globalchallenges.reloadInventory();
        final Map<UUID, Double> placements = globalchallenges.getPlacing(participants);
        if(task != -1) Bukkit.getScheduler().cancelTask(task);
        active.remove(type);
        if(giveRewards) {
            int i = 1;
            for(UUID p : placements.keySet()) {
                final RPPlayer pdata = RPPlayer.get(p);
                if(i <= recordPlacements) {
                    final GlobalChallengePrize prize = GlobalChallengePrize.valueOf(i);
                    pdata.addGlobalChallengePrize(prize);
                    i += 1;
                }
            }
        }
    }

    public static ActiveGlobalChallenge valueOf(ItemStack display) {
        if(display != null && display.hasItemMeta() && display.getItemMeta().hasDisplayName())
            for(ActiveGlobalChallenge g : active.values())
                if(g.getType().getDisplayItem().getItemMeta().getDisplayName().equals(display.getItemMeta().getDisplayName()))
                    return g;
        return null;
    }

    public static void deleteAll() {
        active = null;
        pm = null;
        globalchallenges = null;
    }
}
