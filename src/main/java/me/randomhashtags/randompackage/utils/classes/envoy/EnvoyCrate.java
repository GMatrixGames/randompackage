package me.randomhashtags.randompackage.utils.classes.envoy;

import me.randomhashtags.randompackage.RandomPackageAPI;
import me.randomhashtags.randompackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class EnvoyCrate {
	public static HashMap<String, EnvoyCrate> crates;
	public static String defaultTier;
	private static RandomPackageAPI api;

	private YamlConfiguration yml;
	private String ymlName;
	private int chance;

	private UMaterial block, fallingblock;
	private String rewardSize;
	private List<String> rewards;
	private List<UMaterial> cannotLandAbove;
	private boolean dropsFromSky, canRepeatRewards;
	private ItemStack item;
	private Firework fw;

	public EnvoyCrate(YamlConfiguration yml, String ymlName) {
		if(crates == null) {
			crates = new HashMap<>();
			api = RandomPackageAPI.getAPI();
		}
		this.yml = yml;
		this.ymlName = ymlName.split("\\.yml")[0];
		this.chance = yml.getInt("chance");
		this.dropsFromSky = yml.getBoolean("settings.drops from sky");
		this.canRepeatRewards = yml.getBoolean("settings.can repeat rewards");

		crates.put(ymlName, this);
	}

	public void delete() {
		crates.remove(ymlName);
		yml = null;
		ymlName = null;
		chance = 0;
		block = null;
		fallingblock = null;
		rewardSize = null;
		rewards = null;
		cannotLandAbove = null;
		item = null;
		fw = null;

		if(crates.isEmpty()) {
			crates = null;
			defaultTier = null;
			api = null;
		}
	}

	public YamlConfiguration getYaml() { return yml; }
	public String getYamlName() { return ymlName; }

	private void loadFirework() {
		if(fw == null) {
			final String[] f = yml.getString("firework").split(":");
			fw = api.createFirework(FireworkEffect.Type.valueOf(f[0].toUpperCase()), api.getColor(f[1]), api.getColor(f[2]), Integer.parseInt(f[3]));
		}
	}
	public Firework getFirework() {
		loadFirework();
		return fw;
	}
	public int getChance() { return chance; }
	private void loadBlock() {
		if(block == null) {
			block = UMaterial.match(yml.getString("settings.block"));
		}
	}
	public UMaterial getBlock() {
		loadBlock();
		return block;
	}
	public boolean dropsFromSky() { return dropsFromSky; }
	private void loadFallingblock() {
		if(fallingblock == null) fallingblock = UMaterial.match(yml.getString("settings.falling block"));
	}
	public UMaterial getFallingBlock() {
		loadFallingblock();
		return fallingblock;
	}
	public boolean canRepeatRewards() { return canRepeatRewards; }
	private void loadRewardSize() {
		if(rewardSize == null) rewardSize = yml.getString("settings.reward size");
	}
	public String getRewardSize() {
		loadRewardSize();
		return rewardSize;
	}
	private void loadCannotLandAbove() {
		if(cannotLandAbove == null) {
			cannotLandAbove = new ArrayList<>();
			for(String s : yml.getStringList("settings.cannot land above")) {
				cannotLandAbove.add(UMaterial.match(s));
			}
		}
	}
	public List<UMaterial> cannotLandAbove() {
		loadCannotLandAbove();
		return cannotLandAbove;
	}
	private void loadItem() {
		if(item == null) item = api.d(yml, "item");
	}
	public ItemStack getItem() {
		loadItem();
		return item.clone();
	}
	private void loadRewards() {
		if(rewards == null) rewards = yml.getStringList("rewards");
	}
	public List<String> getRewards() {
		loadRewards();
		return rewards;
	}

	public int getRandomRewardSize() {
		loadRewardSize();
		final String[] s = rewardSize.split("-");
		final boolean c = rewardSize.contains("-");
		final int min = c ? Integer.parseInt(s[0]) : Integer.parseInt(rewardSize), max = c ? Integer.parseInt(s[1]) : -1;
		return min+(max == -1 ? 0 : new Random().nextInt(max-min+1));
	}
	public List<String> getRandomRewards() {
		loadRewards();
		final List<String> rewards = new ArrayList<>(this.rewards), actualrewards = new ArrayList<>();
		final Random random = new Random();
		for(int i = 1; i <= getRandomRewardSize(); i++) {
			if(rewards.size() != 0) {
				final String reward = rewards.get(random.nextInt(rewards.size()));
				final String[] a = reward.split(";chance=");
				if(random.nextInt(100) <= api.getRemainingInt(a[1])) {
					actualrewards.add(a[0]);
					if(!canRepeatRewards) rewards.remove(reward);
				} else {
					i -= 1;
				}
			}
		}
		return actualrewards;
	}
	public List<ItemStack> getRandomizedRewards() {
		final List<String> r = getRandomRewards();
		final List<ItemStack> a = new ArrayList<>();
		for(String s : r) {
			a.add(api.d(null, s));
		}
		return a;
	}
	public boolean canLand(Location spawnLocation) {
		final World w = spawnLocation.getWorld();
		final Block b = w.getBlockAt(new Location(w, spawnLocation.getBlockX(), spawnLocation.getBlockY()-1, spawnLocation.getBlockZ()));
		if(cannotLandAbove().contains(UMaterial.match(b.getType().name()))) return false;
		return true;
	}
	public static EnvoyCrate valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta())
			for(EnvoyCrate c : crates.values())
				if(is.isSimilar(c.getItem()))
					return c;
		return null;
	}
	public static EnvoyCrate getRandomCrate(boolean useChances) {
		final Random random = new Random();
		if(useChances) {
			for(EnvoyCrate c : crates.values())
				if(random.nextInt(100) <= c.getChance())
					return c;
		} else {
			return crates.get(defaultTier);
		}
		return crates.get(defaultTier);
	}
}
