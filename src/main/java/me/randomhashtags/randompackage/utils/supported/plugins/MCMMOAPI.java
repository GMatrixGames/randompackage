package me.randomhashtags.randompackage.utils.supported.plugins;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import com.gmail.nossr50.events.skills.abilities.McMMOPlayerAbilityActivateEvent;
import me.randomhashtags.randompackage.api.GlobalChallenges;
import me.randomhashtags.randompackage.utils.CustomEnchantUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MCMMOAPI extends GlobalChallenges implements Listener {
	public boolean isEnabled = false;
	private static MCMMOAPI instance;
	public static final MCMMOAPI getMCMMOAPI() {
		if(instance == null) instance = new MCMMOAPI();
		return instance;
	}

	private CustomEnchantUtils customenchants;
	public YamlConfiguration itemsConfig;
	public ItemStack creditVoucher, levelVoucher, xpVoucher;
	public boolean gcIsEnabled = false;

	public void enable() {
		if(isEnabled) return;
		customenchants = CustomEnchantUtils.getCustomEnchantUtils();
		isEnabled = true;
		pluginmanager.registerEvents(this, randompackage);
		itemsConfig = YamlConfiguration.loadConfiguration(new File(rpd, "items.yml"));
		gcIsEnabled = GlobalChallenges.getChallenges().isEnabled;
		creditVoucher = d(itemsConfig, "mcmmo vouchers.credit");
		levelVoucher = d(itemsConfig, "mcmmo vouchers.level");
		xpVoucher = d(itemsConfig, "mcmmo vouchers.xp");
	}
	public void disable() {
		if(!isEnabled) return;
		customenchants = null;
		itemsConfig = null;
		creditVoucher = null;
		levelVoucher = null;
		xpVoucher = null;
		isEnabled = false;
		HandlerList.unregisterAll(this);
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void mcmmoPlayerXpGainEvent(McMMOPlayerXpGainEvent event) {
		if(!event.isCancelled()) {
			final Player player = event.getPlayer();
			customenchants.procPlayerArmor(event, player);
			customenchants.procPlayerItem(event, player, null);
			
			if(gcIsEnabled) {
				final UUID p = player.getUniqueId();
				final String skill = event.getSkill().name().toLowerCase();
				final float xp = event.getRawXpGained();
				increase(event, "mcmmoxpgained", p, xp);
				increase(event, "mcmmoxpgainedin_" + skill, p, xp);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void mcmmoAbilityActivateEvent(McMMOPlayerAbilityActivateEvent event) {
		if(!event.isCancelled() && gcIsEnabled) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "mcmmoabilityused", player, 1);
			increase(event, "mcmmoabilityused_" + event.getAbility().name(), player, 1);
		}
	}
	public SkillType getSkill(String skillname) {
		for(SkillType type : SkillType.values()) {
			final String s = itemsConfig.getString("mcmmo-vouchers.skill-names." + type.name().toLowerCase().replace("_skills", ""));
			if(s != null && skillname.equalsIgnoreCase(ChatColor.stripColor(s)) || skillname.equalsIgnoreCase(type.name())) return type;
		}
		return null;
	}
	public String getSkillName(SkillType skilltype) {
		final String a = itemsConfig.getString("mcmmo-vouchers.skill-names." + skilltype.name().toLowerCase().replace("_skills", ""));
		return a != null ? ChatColor.translateAlternateColorCodes('&', a) : null;
	}
	
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ItemStack it = event.getItem();
		if(it == null || it.getType().equals(Material.AIR) || !it.hasItemMeta() || !it.getItemMeta().hasDisplayName() || !it.getItemMeta().hasLore()) {
			return;
		} else {
			final ItemMeta m = it.getItemMeta();
			final String d = m.getDisplayName();
			final boolean credit = d.equals(creditVoucher.getItemMeta().getDisplayName()), level = d.equals(levelVoucher.getItemMeta().getDisplayName()), xpv = d.equals(xpVoucher.getItemMeta().getDisplayName());
			if(credit || level || xpv) {
				final Player player = event.getPlayer();
				int numberslot = -1, skillslot = -1;
				final String itemtype = credit ? "credit" : level ? "level" : "xp";
				final List<String> a = itemsConfig.getStringList("mcmmo vouchers." + itemtype + ".lore"), msg = itemsConfig.getStringList("mcmmo vouchers.messages.redeem " + itemtype);
				for(int i = 0; i < a.size(); i++) {
					if(a.get(i).contains("{AMOUNT}")) numberslot = i;
					if(a.contains("{SKILL}"))  skillslot = i;
				}
				if(numberslot == -1 || skillslot == -1) return;
				final String target = m.getLore().get(skillslot), o = ChatColor.translateAlternateColorCodes('&', itemsConfig.getStringList("mcmmo vouchers." + itemtype + ".lore").get(skillslot));
				com.gmail.nossr50.datatypes.skills.SkillType typee = null;
				for(SkillType type : SkillType.values())
					if(target.equals(o.replace("{SKILL}", "" + getSkillName(type))))
						typee = type;

				int xp = getRemainingInt(m.getLore().get(numberslot));
				event.setCancelled(true);
				player.updateInventory();
				final HashMap<String, String> replacements = new HashMap<>();
				replacements.put("{XP}", formatInt(xp));
				replacements.put("{AMOUNT}", formatInt(xp));
				replacements.put("{SKILL}", typee.name());
				sendStringListMessage(player, msg, replacements);
				if(xpv)          ExperienceAPI.addRawXP(player, typee.name(), xp);
				else if(level) ExperienceAPI.addLevel(player, typee.name(), xp);
				else return;
				removeItem(player, it, 1);
				playSound(itemsConfig, "mcmmo vouchers.sounds.redeem " + itemtype, player, player.getLocation(), false);
			}
		}
	}
}